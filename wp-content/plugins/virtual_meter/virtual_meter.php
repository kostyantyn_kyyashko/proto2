<?
/*
 *	Plugin Name: Virtual Meter
 *  Author: Relotis
 *  Description: meter with scale for crm
 *
 * License: GPLv3 or later
 * License URI: https://www.gnu.org/licenses/gpl-3.0.en.html
 * Version: 1.0.0
 * Text Domain: relotis-crm
 */
function virtual_meter($atts)
{
    $parameters = [
        'value' => 20,
        'max_value'=> 100,
        'width' => 200,
        'height' => 200,
        'main_container_id' => 'gaugeContainer'
    ];
    if (is_array($atts) && count($atts) > 0) {
        foreach ($parameters as $key => $parameter_value) {
            if (isset($atts[$key])) {
                $parameters[$key] = $atts[$key];
            }
        }
    }
    $delta = round($parameters['max_value']/6);
    ?>
<link rel="stylesheet" href="<?=plugin_dir_url(__FILE__)?>css/jqx.base.css" type="text/css" />
<script type="text/javascript" src="<?=plugin_dir_url(__FILE__)?>js/jqxcore.js"></script>
<script type="text/javascript" src="<?=plugin_dir_url(__FILE__)?>js/jqxdraw.js"></script>
<script type="text/javascript" src="<?=plugin_dir_url(__FILE__)?>js/jqxgauge.js"></script>
<link rel="stylesheet" href="<?=plugin_dir_url(__FILE__)?>css/gaugeValue.css" type="text/css" />
<script>
    var delta = <?=$delta?>;
    var ranges= [{ startValue: 0, endValue: delta, style: { fill: '#e02629', stroke: '#e02629' }, endWidth: 10, startWidth: 10 },
        { startValue: delta , endValue: delta*2, style: { fill: '#ff8000', stroke: '#ff8000' }, endWidth: 10, startWidth: 10 },
        { startValue: delta*2, endValue: delta*3, style: { fill: '#fbd109', stroke: '#fbd109' }, endWidth: 10, startWidth: 10 },
        { startValue: delta*3, endValue: delta*4, style: { fill: '#a6e091', stroke: '#83E077' }, endWidth: 10, startWidth: 10 },
        { startValue: delta*4, endValue: delta*5, style: { fill: '#4bb648', stroke: '#4bb648' }, endWidth: 10, startWidth: 10 },
        { startValue: delta*5, endValue:<?=round($parameters['max_value'])?>, style: { fill: '#4bb648', stroke: '#4bb648' }, endWidth:10, startWidth: 10 }
    ];
        $(document).ready(function () {
            $('#<?=$parameters['main_container_id']?>').jqxGauge({
                ranges: ranges,
                ticksMinor: { interval: 5, size: '5%' },
                ticksMajor: { interval: 10, size: '9%' },
                value: 0,
                colorScheme: 'scheme05',
                endAngle: 270,
                animationDuration: 1200,
                max: <?=$parameters['max_value']?>,
                width: <?=$parameters['width']?>,
                height: <?=$parameters['height']?>,
            });

            $('#<?=$parameters['main_container_id']?>').on('valueChanging', function (e) {
                $('#gaugeValue').remove();
            });

            $('#<?=$parameters['main_container_id']?>').jqxGauge('value', <?=$parameters['value']?>);

        });
</script>
    <div id="demoWidget" style="position: relative;">
        <div style="float: left;" id="gaugeContainer"></div>
        <div id="gaugeValue" style="position: absolute; top: 235px; left: 132px; font-family: Sans-Serif; text-align: center; font-size:1px !important; width: 70px;"></div>
        <div style="margin-left: 60px; float: left;" id="linearGauge"></div>
    </div>


    <div style="position: absolute; bottom: 5px; right: 5px; display: none;">
        <a href="https://www.jqwidgets.com/" alt="https://www.jqwidgets.com/"><img alt="https://www.jqwidgets.com/" title="https://www.jqwidgets.com/" src="https://www.jqwidgets.com/wp-content/design/i/logo-jqwidgets.png"/></a>
    </div>
    <?
}
add_shortcode('virtual_meter', 'virtual_meter');