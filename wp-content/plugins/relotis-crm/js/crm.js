function add_contact() {
    $('#add_contact').click(function () {
        var name = $('[name=name]').val(),
            company = $('[name=company]').val(),
            position = $('[name=position]').val(),
            phone = $('[name=phone]').val(),
            email = $('[name=email]').val();
        $.ajax({
            url: ajaxurl,
            type: 'post',
            data: {
                action: 'add_contact_to_db',
                transaction_id: $.cookie('transaction_id'),
                name: name,
                company: company,
                position: position,
                phone: phone,
                email: email,
            },
            success: function (resp) {
                if (resp === 'ok') {
                    window.location.reload();
                }
                else {
                    alert('Contact add: FAIL');
                }
            }
        });
    });
}
function remove_contact() {
    $('.remove_contact').click(function () {
        if (confirm('Deleting a contact: are you sure?')) {
            var id = $(this).attr('id');
            $.ajax({
                url: ajaxurl,
                type: 'post',
                data: {
                    action: 'remove_contact_from_db',
                    id: id
                },
                success: function (resp) {
                    if (resp === 'ok') {
                        window.location.reload();
                    }
                    else {
                        alert('Remove contact: Fail')
                    }
                }
            })
        }
    })
}

function analysis_refresh_data() {
    $('#analysis_refresh_data').unbind();
    $('#analysis_refresh_data').click(function () {
        var stage = $(this).attr('data-stage');
        var begin_at=$('[name=date2]').val();
        $.ajax({
            url: ajaxurl,
            type: 'post',
            data: {
                action: 'analysis_refresh_data',
                transaction_id: $.cookie('transaction_id'),
                crm_owner: $('#crm_owner').val(),
                crm_comment: $('textarea#crm_comment').val(),
                transaction_name: $('#transaction_name').val(),
                begin_at: begin_at
            },
            success: function (resp) {
                if (resp === 'ok') {
                    alert('Data refresh: SUCCESS');
                    window.location.reload();
                }
                else {
                    alert('Data refresh: FAIL')
                }
            }
        })
    });
}

function proposal_refresh_data() {
    $('#proposal_refresh_data').click(function () {
       // var product_id = $('[id*=product-]').attr('id'); alert(product_id); return false;
        $.ajax({
            url: ajaxurl,
            type: 'get',
            data: {
                action: 'proposal_refresh_data',
                transaction_id: $.cookie('transaction_id'),
                proposal_date: $('[data=proposal_date]').val(),
                proposal_time: $('[data=proposal_time]').val(),
                proposal_whom: $('#proposal_whom').val(),
                proposal_text: $('#proposal_text').val(),
            },
            success: function (resp) { console.log(resp);
                if (resp === 'ok') {
                    alert('Data refresh: SUCCESS')
                    window.location.reload();
                }
                else {
                    alert('Data refresh: FAIL')
                }
            }
        })
    });
}

function negotiation_refresh_data() {
    $('#negotiation_refresh_data').click(function () {
        $.ajax({
            url: ajaxurl,
            type: 'post',
            data: {
                action: 'negotiation_refresh_data',
                transaction_id: $.cookie('transaction_id'),
                date: $('[data-nego="date"]').val(),
                time: $('[data-nego="time"]').val(),
                person_present: $('[data-nego="person_present"]').val(),
                comment: $('[data-nego="comment"]').val(),
                result: $('[data-nego="result"]').val(),
            },
            success: function (resp) {console.log(resp);
                if (resp === 'ok') {
                    window.location.reload();
                }
                else {
                    alert('Data refresh: FAIL')
                }
            }
        })
    });
}

function set_stage_as_complete() {
    $('.stage_complete').click(function () {
        var stage = $.cookie('stage');
        $.ajax({
            url: ajaxurl,
            type: 'post',
            data: {
                action: 'set_stage_as_complete',
                transaction_id: $.cookie('transaction_id'),
                stage: stage,
                status: 'completed'
            },
            success: function (resp) {
                    if (resp === 'ok') {
                        alert('Stage ' + stage + ' Completed: OK');
                        window.location.reload();
                }
                else {
                        alert(resp);
                }
            }
        })
    });
}

function set_transaction_complete() {
    $('#transaction_close_button').click(function () {
        var transaction_id = $.cookie('transaction_id'),
            close_at = $('#close_at').val();
        $.ajax({
            url: ajaxurl,
            type: 'post',
            data: {
                action: 'set_transaction_complete',
                transaction_id: transaction_id,
                close_at: close_at,
                price_value: $('#price_value').val()
            },
            success: function (resp) {
                if (resp === 'ok') {
                    alert('Transaction Successfully closed');
                    window.location.reload()
                }
            }

        })

    });
}


function set_svg_color() {
    $('.jet-tabs__control').click(function () {
        var stage = $(this).find('.elementor-clearfix').eq(0).text();
        $('.jet-tabs__control').find('.elementor-clearfix').css({color: '#fff'});
        $(this).find('.elementor-clearfix').css({color: '#000'});
        $('svg').css({color: '#61CE70'});
        $(this).find('svg').css({color: 'yellow'})
    })
}

function remove_complete_button() {
    $('#jet-tabs-control-1006').remove();
}


function set_stages_element_disabled() {
    if (window.location.href.indexOf('ulcrm') > 0) {
        setTimeout(function () {
            var stages= $('.stage_wrapper'),
                transaction_id = $.cookie('transaction_id');
            $.each(stages, function (index, stage_container) {
                var stage = $(stage_container).attr('data-stage');
                $.ajax({
                    url: ajaxurl,
                    type: 'post',
                    data: {
                        action: 'check_stage_as_complete',
                        transaction_id: transaction_id,
                        stage: stage
                    },
                    success: function (resp) {
                        if(resp === 'complete') {
                            $(stage_container).find('textarea').attr('disabled', true);
                            $(stage_container).find('input').attr('disabled', true);
                            $(stage_container).find('button').attr('disabled', true);
                        }
                    }
                })
            })
        }, 500);
    }
}

function remember_active_tab(){
    $('div.jet-tabs__control').click(function () {
        $.cookie('div_stage_button_id', $(this).attr('id'), { expires: 7, path: '/' });
        var stage = $(this).find('.elementor-text-editor').eq(0).text();
        $.cookie('stage', stage, { expires: 7, path: '/' });
    });
}

function restore_active_tab() {
    if (window.location.href.indexOf('ulcrm') > 0 ){
        setTimeout(function () {
            button_clicker($.cookie('div_stage_button_id'));
        }, 200)
    }
}

function button_clicker(div_id) {
    setTimeout(function () {
        if (!div_id) {
            $('#jet-tabs-control-1001').find('.elementor-text-editor').click();
        }
        else {
            $('#' + div_id).find('.elementor-text-editor').click();
        }
    }, 200);
}

function go_to_hash() {
    var hash = window.location.hash.substring(1);
    var div_ids = {
        analysis: 'jet-tabs-control-1001',
        contacts: 'jet-tabs-control-1002',
        proposal: 'jet-tabs-control-1003',
        negotiation: 'jet-tabs-control-1004',
        close: 'jet-tabs-control-1005',
    };
    setTimeout(function () {
        $('#' + div_ids[hash]).find('.elementor-text-editor').click();
    }, 500);
}

function save_task() {
    $('.save_task').click(function () {
        var stage = $.cookie('stage'),
            transaction_id = $.cookie('transaction_id'),
            task = $(this).parent().parent().parent().find('.task').val(),
            executor = $(this).parent().parent().parent().find('.executor').val(),
            date = $(this).parent().parent().parent().find('.date').val(),
            time = $(this).parent().parent().parent().find('.time').val(),
            result = $(this).parent().parent().parent().find('.result').val();
        $.ajax({
            url: ajaxurl,
            type: 'post',
            data: {
                action: 'save_task',
                stage: stage,
                transaction_id: transaction_id,
                task: task,
                executor: executor,
                date: date,
                time: time,
                result: result
            },
            success: function (resp) {console.log(resp);
                if (resp === 'ok') {
                    window.location.reload();
                }
                else {
                    alert('Task Save: FAIL')
                }
            }
        })
    });
}

function transfer_to_crm()
{
        var price = $('.woocommerce-Price-amount').eq(0).text(),
            address = $('.woocommerce-product-details__short-description').eq(0).find('h4').text(),
            description = $('.woocommerce-product-details__short-description').eq(0).find('p').text(),
            image_src = $('.woocommerce-product-gallery__wrapper').find('a').attr('href'),
            object_id = $('[id*=product-]').attr('id');
        $('#crm_button').unbind();
        $('#crm_button').click(function (e) {
            e.preventDefault();
            $.ajax({
                url: ajaxurl,
                type:'post',
                dataType: 'json',
                data: {
                    action: 'init_transaction',
                    wp_user_id: wp_user_id,
                    wp_user_name: wp_user_name,
                    price: price,
                    address: address,
                    description: description,
                    image_src: image_src,
                    object_url: window.location.href,
                    object_id: object_id
                },
                success: function (resp) {
                    if (resp.status === 'ok') {
                        $.cookie('transaction_id', resp.transaction_id,  { expires: 30, path: '/' });
                        $.cookie('wp_user_id', resp.wp_user_id,  { expires: 30, path: '/' });
                        $.cookie('wp_user_name', resp.wp_user_name,  { expires: 30, path: '/' });
                        window.location.href = '/ulcrm/'
                    }}
            });
            return false;
        });
}

function save_user_plan() {
    $('[data-user-id]').unbind();
    $('[data-user-id]').click(function () {
        var id = $(this).attr('data-user-id');
        var plan = $(this).parent().parent().find('input.plan').val();
       $.ajax({
           url: ajaxurl,
           type: 'post',
           data: {
               action: 'save_user_plan',
               id: id,
               plan: plan
           },
           success: function (resp) {
               if (resp == 'ok') {
                   alert('Saving plan: SUCCESS');
                   window.location.reload();
               }
               else {
                   alert('Saving plan: FAIL')
               }
           }
       })

    })
}
var waiting_iteration;

function wait_for_meter_ready() {
    if(window.location.href.indexOf('ulcrm') > 0) {
        waiting_iteration = setInterval(function(){
            var raw = ($('#demoWidget').find('.tdTop').html()); console.log(raw);
            if (typeof raw != 'undefined') {
                $('#demoWidget').find('.tdTop').html('<div style="">Sales Target: <input disabled id="sales_target" value='+plan+' style="width: 100px;"></div>');
                stop_waiting();
            }
        }, 1000);
    }
}

function stop_waiting() {
    clearInterval(waiting_iteration);
}

function add_crm_button(){
    $('#crm_button').remove()
    var html = '<button id="crm_button" class="btn btn-danger">Transfer to CRM</button>';
    $('.product_meta').append(html);
    setTimeout(function () {
        transfer_to_crm();
    }, 200);
}

$(document).ready(function () {
    //transfer_to_crm();
    add_contact();
    remove_contact();
    remove_complete_button();
    set_svg_color();
    analysis_refresh_data();
    proposal_refresh_data();
    negotiation_refresh_data();
    set_stage_as_complete();
    set_stages_element_disabled();
    remember_active_tab();
    restore_active_tab();
    go_to_hash();
    set_transaction_complete();
    if (window.location.href.indexOf('ulcrm') > 0) {
        setTimeout(function () {
            $(function() {
                $(".datepicker").datepicker({
                    format: 'mm/dd/yyyy',
                    startDate: '-3d'
                });
            });
        }, 500);
    }
    save_task();
    save_user_plan();
    wait_for_meter_ready();

});


