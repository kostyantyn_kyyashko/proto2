<?php

if(!isset($_GET['relotis'])){

    add_action( 'admin_menu', 'remove_menus' );
}

function remove_menus(){
    remove_menu_page( 'index.php' );                  //Консоль
    remove_menu_page( 'edit.php' );                   //Записи
    remove_menu_page( 'upload.php' );                 //Медиафайлы
    remove_menu_page( 'edit.php?post_type=page' );    //Страницы
    remove_menu_page( 'edit-comments.php' );          //Комментарии
    remove_menu_page( 'themes.php' );                 //Внешний вид
    remove_menu_page( 'plugins.php' );                //Плагины
    //remove_menu_page( 'users.php' );                  //Пользователи
    remove_menu_page( 'tools.php' );
    remove_menu_page( 'options-general.php' );
    remove_menu_page( 'admin.php?page=wpcf7' );
    remove_menu_page( 'admin.php?page=unlimitedelements' );
    remove_menu_page( 'edit.php?post_type=elementor_library&tabs_group=library' );
    remove_menu_page( 'admin.php?page=wc-admin&path=/marketing' );
    add_menu_page('Relotis', 'Go to Site', 'read', 'go_to_site', 'go_to_site');
    function go_to_site()
    {
        header("Location: /");
    }
    add_menu_page('Listins', 'Listings', 'read', 'listings', 'listing');
    remove_menu_page('listings');
    if (wp_get_current_user()->roles[0] !== 'administrator') {
        //remove_menu_page('users.php');
        ?><style>#menu-users{display:none}</style><?
        ?><style>#toplevel_page_stat{display:none}</style><?
    }
    ?>
    <style>
        .notice,
        .woocommerce-layout,
        #screen-meta-links,
        #toplevel_page_jet-dashboard,
        #toplevel_page_piotnet-addons-for-elementor,
        #menu-posts-elementor_library,
        #toplevel_page_wp-mail-smtp,
        #toplevel_page_unlimitedelements,
        #toplevel_page_wc-admin-path--analytics-revenue,
        #toplevel_page_wc-admin-path--marketing,
        #wp-toolbar,
        #toplevel_page_asp_settings,
        #toplevel_page_wpcf7,
        #toplevel_page_mailchimp-for-wp,
        #toplevel_page_woocommerce,
        #toplevel_page_elementor,
        #toplevel_page_wpfastestcacheoptions,
        #wpadminbar,
        #toplevel_page_charts
         {display: none;}
        #adminmenu {{display: block}}
        #adminmenuwrap {margin-top: 30px !important;}
    </style>
    <script
            src="https://code.jquery.com/jquery-3.5.0.min.js"
            integrity="sha256-xNzN2a4ltkB44Mc/Jz3pT4iU1cmeR0FkXs4pru/JxaQ="
            crossorigin="anonymous"></script>

    <script>

        $(document).ready(function () {
            setTimeout(function () {
                $('#menu-posts-product').find('.wp-menu-name').text('Listings');
                $('#menu-posts-product').find('.wp-first-item').text('All Listings')
                $('#adminmenuback').append('<div ib="bob" style="position: absolute; top: 0; left:0;">' +
                        '<a href="/">'+
                    '<img style="height: 150px; margin-top: 10px;" src="<?=plugin_dir_url(__FILE__)?>img/logo.png"></a></div>');
            }, 100);
        });
    </script>

    <?

}
show_admin_bar(false);
add_action('admin_menu', 'relotis_support_page_init2' );
function relotis_support_page_init2()
{
    add_menu_page('Support', 'Support', 'read', 'support', 'relotis_support_page2');
}
function relotis_support_page2()
{
    if (isset($_POST['message'])) {
        echo "<h1>Thank you, we will contact you as soon as possible 24 hours</h1>";
        wp_mail('support@relotis.com ', 'Help to ' . $_POST['name'] . '', $_POST['message']);
        return;
    }
    $user = _wp_get_current_user();
    ?>
    <h1>Relotis support:</h1>
    <h2>By email: support@relotis.com</h2>
    <h2>By phone: +1 234 5678912345</h2>
    <h2> Or fill this form</h2>
    <form method="post" action="">
        <label for="user_login"><h3>Your login</h3>
            <input type="text" class="form-control" name="name" value="<?=$user->user_login?>" id="user_login" style="width: 250px;">
        </label>
        <br>
        <label for="user_email"> <h3>Your email</h3>
            <input type="text" class="form-control" name="email" value="<?=$user->user_email?>" id="user_email" style="width: 250px;">
        </label>
        <br>
        <label for="user_message"> <h3>Message to support</h3>
            <textarea class="form-control" name="message" id="user_message" placeholder="Put your message there" style="width: 500px;"></textarea>
        </label>
        <br>
        <button class="btn btn-info" type="submit">Send message to support</button>
    </form>
    <?
}

add_action('admin_menu', 'relotis_logout_page' );
function relotis_logout_page()
{
    add_menu_page('LogOut', 'Logout', 'read', 'logout', 'logout_user');
}
function logout_user()
{
    wp_logout();
}
